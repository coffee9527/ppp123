<?php

return [

    'pdo' => [
        'driver' => 'mysql',
        'host' => '10.10.220.8',
        'db_name' => 'tibame',
        'db_username' => 'tibame',
        'db_user_password' => 'tibame',
        'default_fetch' => PDO::FETCH_OBJ,
    ],
    'mysqli' => [
        'host' => '10.10.220.8',
        'db_name' => 'tibame',
        'db_username' => 'tibame',
        'db_user_password' => 'tibame',
        'default_fetch' => MYSQLI_ASSOC,
    ],
];
